module gitlab.com/searsaw/btc-cost-basis

go 1.16

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0
	github.com/leekchan/accounting v1.0.0
)
