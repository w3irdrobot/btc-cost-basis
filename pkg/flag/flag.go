package flag

import (
	"fmt"
	"strings"

	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
)

// SliceFlag represents a flag that can be repeated
// on the command line to pass multiple values
type SliceFlag struct {
	kind   data.Source
	Values []Value
}

type Value struct {
	Kind  data.Source
	Value string
}

func NewSliceFlag(kind data.Source) SliceFlag {
	return SliceFlag{kind: kind}
}

func (s *SliceFlag) String() string {
	b := strings.Builder{}

	for _, v := range s.Values {
		out := fmt.Sprintf("%s: %s", v.Kind, v.Value)
		b.WriteString(out)
	}

	return b.String()
}

func (s *SliceFlag) Set(flag string) error {
	v := Value{Kind: s.kind, Value: flag}
	s.Values = append(s.Values, v)
	return nil
}
