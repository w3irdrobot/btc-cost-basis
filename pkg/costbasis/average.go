package costbasis

import (
	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
)

const (
	MethodAverage = "acb"
)

func Average(purchaseData []data.Data) (float64, error) {
	totalBtc := float64(0)
	totalPaid := float64(0)

	for _, d := range purchaseData {
		totalBtc += float64(d.AmountPurchased)
		totalPaid += float64(d.AmountPaid)
	}
	// cents per sat
	basis := totalPaid / totalBtc

	return basis, nil
}
