package costbasis

import (
	"fmt"

	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
)

type Method string

type InvalidMethod string

func (i InvalidMethod) Error() string {
	return fmt.Sprintf("invalid method %s", string(i))
}

type Algorithm func([]data.Data) (float64, error)

var methods map[Method]Algorithm = map[Method]Algorithm{
	MethodAverage: Average,
}

func Methods() []string {
	m := make([]string, 0, len(methods))

	for k := range methods {
		m = append(m, string(k))
	}

	return m
}

func ValidMethod(m string) bool {
	_, ok := methods[Method(m)]

	return ok
}

func MethodAlgorithm(m string) (Algorithm, error) {
	if !ValidMethod(m) {
		return nil, InvalidMethod(m)
	}

	return methods[Method(m)], nil
}
