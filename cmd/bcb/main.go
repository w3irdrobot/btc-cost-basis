package main

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"

	"github.com/leekchan/accounting"

	"gitlab.com/searsaw/btc-cost-basis/pkg/costbasis"
	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
	"gitlab.com/searsaw/btc-cost-basis/pkg/data/gemini"
	"gitlab.com/searsaw/btc-cost-basis/pkg/data/swan"
	bcbflag "gitlab.com/searsaw/btc-cost-basis/pkg/flag"
)

var (
	Version     string
	swanCSVs    = bcbflag.NewSliceFlag(data.SourceSwan)
	geminiExcel = bcbflag.NewSliceFlag(data.SourceGemini)
)

func main() {
	flag.Usage = func() {
		usage := "bcb [--method method] [--swan path/to/file]... [--gemini path/to/file]..."
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "%s\n", usage)
		flag.PrintDefaults()
	}

	validMethods := strings.Join(costbasis.Methods(), ", ")

	version := flag.Bool("version", false, "display the CLI version")
	method := flag.String("method", string(costbasis.MethodAverage),
		fmt.Sprintf("the method of calculating the cost basis (%s)", validMethods))
	flag.Var(&swanCSVs, "swan", "a list of CSVs from Swan Bitcoin")
	flag.Var(&geminiExcel, "gemini", "a list of Excel files from Gemini")
	flag.Parse()

	if *version {
		fmt.Println(Version)
		os.Exit(0)
	}

	if !costbasis.ValidMethod(*method) {
		fmt.Printf("the method %s is not valid\n\n", *method)
		flag.Usage()
		os.Exit(1)
	}

	var purchaseData []data.Data
	files := append(swanCSVs.Values, geminiExcel.Values...)

	if len(files) == 0 {
		flag.Usage()
		os.Exit(0)
	}

	for _, file := range files {
		fd, err := os.Open(file.Value)
		if err != nil {
			fmt.Printf("error opening file %s: %s\n", file.Value, err)
			os.Exit(1)
		}

		var d []data.Data

		switch file.Kind {
		case data.SourceSwan:
			d, err = swan.Parse(fd)
		case data.SourceGemini:
			d, err = gemini.Parse(fd)
		default:
			err = data.ErrUnknownSource{Source: string(file.Kind)}
		}

		if err != nil {
			fmt.Printf("error parsing data in file %s: %s\n", file.Value, err)
			os.Exit(1)
		}

		purchaseData = append(purchaseData, d...)
	}

	sort.SliceStable(purchaseData, func(i, j int) bool {
		return purchaseData[i].Date.Before(purchaseData[j].Date)
	})

	algorithm, err := costbasis.MethodAlgorithm(*method)
	if err != nil {
		fmt.Printf("error getting the method algorithm: %s\n\n", err)
		flag.Usage()
		os.Exit(1)
	}

	// cents per sat
	basisCents, err := algorithm(purchaseData)
	if err != nil {
		fmt.Printf("error calculating the cost basis with method '%s': %s\n\n", *method, err)
		flag.Usage()
		os.Exit(1)
	}
	// dollars per bitcoin
	basis := basisCents * (100_000_000 / 100)

	dateFormat := "Mon Jan 2 2006 15:04:05 EST"
	earliestDate := purchaseData[0].Date.Format(dateFormat)
	latestDate := purchaseData[len(purchaseData)-1].Date.Format(dateFormat)
	basisFormat := accounting.DefaultAccounting("$", 2).FormatMoneyFloat64(basis)

	tabbing := tabwriter.NewWriter(os.Stdout, 0, 8, 0, '\t', 0)
	_, _ = fmt.Fprintf(tabbing, "number of transactions\t%d\n", len(purchaseData))
	_, _ = fmt.Fprintf(tabbing, "earliest transaction\t%s\n", earliestDate)
	_, _ = fmt.Fprintf(tabbing, "latest transaction\t%s\n", latestDate)
	_, _ = fmt.Fprintf(tabbing, "cost basis\t%s\n", basisFormat)
	_ = tabbing.Flush()
}
